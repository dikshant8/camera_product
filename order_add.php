<!DOCTYPE html><!-- Order PHP DATABASE STORING -->
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<meta name="description" content="Internet Technologies Assignment-3" />
		<meta name="keywords" content="HTML, PHP" />
		<meta name="author" content="Dikshant Bawa" />
		<link href= "styles/index.css" rel="stylesheet" type="text/css" />
		<link href= "styles/form.css" rel="stylesheet" type="text/css" />
		<script src="product.js" ></script>
		<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
		<script src="alternative.js" ></script>-->
		<!--  <link rel="stylesheet" type="text/css" media="only screen and (max-device-width: 640px)" href="styles/mobile.css" />  -->
		<title> Canon dSLR Camera </title>
	</head>
	<body>
		<?php
			include ("menu.php");
		?>
		<?php				
			require_once ("settings.php");		
			$conn = @mysqli_connect($host,
					$user,
					$pwd,
					$sql_db
			);
			//checking the connection
			if(!$conn){
				echo "<p> Database connection failure</p>";
			}
			else 
			{
				//getting data from FORM
				$product=trim($_POST["product"]);
				$quantity=trim($_POST["quantity"]);
				$amount=trim($_POST["amount"]);
				$cardname = trim($_POST["cardname"]);
				$cardexpire = trim($_POST["cardexpire"]);
				$cardnumber = trim($_POST["cardnumber"]);
				$mydate = date('Y/m/d H:i:s');
				$sql_table="orders";
		
				//creating table if it doesnt exist
				$sqlString = "show tables like '$sql_table'"; 
				$result1 = @mysqli_query($conn, $sqlString);
				if(mysqli_num_rows($result1)==0) {
					$sqlString = "create table orders (order_id int not null auto_increment primary key,
					c_id int not null,cardname varchar(30) not null,cardnumber varchar(16) not null,cardexpire varchar(5) not null,
					order_date date not null,order_status varchar(20) not null,product varchar(20) not null, quantity int not null,
					amount int not null,FOREIGN KEY (c_id) references customers(customer_id))"; 
					$result2 = @mysqli_query($conn, $sqlString);
				}
				else 
				{					
					//echo "<p>Table  $sql_table already exists</p>"; 
				} 
				$get = mysqli_query($conn, "SELECT customer_id from customers order by customer_id desc limit 1");		
				//checks if connection was successful
				if(!$get){
					echo "<p> Something is wrong with</p>";
				} 
				//inserting the values in orders table
				else{					
					while($row = mysqli_fetch_assoc($get)){
					$id = $row["customer_id"];
					$query="insert into $sql_table (c_id,cardname,cardnumber,cardexpire,order_date,order_status,product,quantity, amount)
					values ('$id','$cardname','$cardnumber','$cardexpire','$mydate','pending','$product','$quantity','$amount' )";
					$result3=mysqli_query($conn, $query);
					if(!$result3)
						echo "<p> Something is wrong with ",$query,"</p>";
					else 
						echo "<p> Successfully added Order</p>";
						echo "<p> Your Customer ID is ",$id," Please note it for future Reference</p>";
				}
				mysqli_free_result($get);}
				mysqli_close($conn);
			}	
		?>	
		<a class="ref" href="purchase.php">Go Back to Previous Page</a> 			
		<?php
			include ("footer.php");
		?>	
	</body>
</html>