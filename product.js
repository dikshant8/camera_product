/**
* Author: Dikshant Bawa
* Purpose: This file is for validation of data for Assignment 2
* Created: 1 May 2014
* Last updated: 5 May 2014
*  
*/


//Function for validating customer.htm
function validate(){
	
	var errMsg = "";								
	var result = true;								
	var dob = document.getElementById("dob").value;
	var postcode = document.getElementById("bpostcode").value;
	var state = document.getElementById("bstate").value;
	
	
	/* Checking of Australian State PostCodes*/
	
	if (state == 'VIC')
	{
		var testPattern = /^[38]\d\d\d/;
		if(( testPattern.test(postcode) )== false)
		{
			errMsg += "Enter a valid Postcode\n";
			result = false;
		}
	}
	else if (state == 'NSW')
	{
		var testPattern = /^[12]\d\d\d/;
		if(( testPattern.test(postcode) )== false)
		{
			errMsg += "Enter a valid Postcode\n";
			result = false;
		}
	}
	else if (state == 'QLD')
	{
		var testPattern = /^[49]\d\d\d/;
		if(( testPattern.test(postcode) )== false)
		{
			errMsg += "Enter a valid Postcode\n";
			result = false;
		}
	}
		
	else if (state == 'NT')
	{
		var testPattern = /^[0]\d\d\d/;
		if(( testPattern.test(postcode) )== false)
		{
			errMsg += "Enter a valid Postcode\n";
			result = false;
		}
	}
	
	else if (state == 'WA')
	{
		var testPattern = /^[6]\d\d\d/;
		if(( testPattern.test(postcode) )== false)
		{
			errMsg += "Enter a valid Postcode\n";
			result = false;
		}
	}
	
	else if (state == 'SA')
	{
		var testPattern = /^[5]\d\d\d/;
		if(( testPattern.test(postcode) )== false)
		{
			errMsg += "Enter a valid Postcode\n";
			result = false;
		}
	}
	
	else if (state == 'TAS')
	{
		var testPattern = /^[7]\d\d\d/;
		if(( testPattern.test(postcode) )== false)
		{
			errMsg += "Enter a valid Postcode\n";
			result = false;
		}
	}
	
	else if (state == 'ACT')
	{
		var testPattern = /^[0]\d\d\d/;
		if(( testPattern.test(postcode) )== false)
		{
			errMsg += "Enter a valid Postcode\n";
			result = false;
		}
	}
	
	
/* Checking the DOB of Person is valid or not*/
	var day = parseInt(dob.substring(0, 2),10);
    var month  = parseInt(dob.substring(3, 5),10);
    var year  = parseInt(dob.substring(6, 10),10);

	if((month < 1) || (month > 12)) 
	{
		errMsg += "Enter a valid Month\n";
		result = false;
	}
	else if((day < 1) || (day > 31)){
		errMsg += "Enter a valid Day\n";
		result = false;
	}
		
	else if((year < 1) || (year > 9999)){
		errMsg += "Enter a valid Year\n";
		result = false;
	}	
	if (errMsg != ""){   
		alert(errMsg);
	}
	if (result){ 
		store_user(dob, state, postcode);
	}
	
	return result;   
}

//Function for storing Customer Details
function store_user(dob,state,postcode){
	
	localStorage.name = document.getElementById("firstname").value +"  " + document.getElementById("lastname").value;
	localStorage.dob = dob;
	localStorage.state = state;
	localStorage.postcode = postcode;
	localStorage.street=document.getElementById("bstreet").value;
	localStorage.suburb=document.getElementById("bsuburb").value;
	localStorage.phone=document.getElementById("phone").value;
	localStorage.email=document.getElementById("email").value;
	
	
}

//Function for validating select.htm
function validate2(){
	var errMsg2 = "";								
	var result2 = true;
	var isBattery = document.getElementById("battery").checked;
	var isTripod = document.getElementById("tripod").checked;
	var isHdmi = document.getElementById("hdmi").checked;
	
	if (!isBattery && !isTripod && !isHdmi) {					
		errMsg2 += "Please select at least one optional accessory.\n";  
		result2 = false;
	}
	if (errMsg2 != ""){   
		alert(errMsg2);
	}
	if (result2){ 
		store_user2();
	}
	return result2;
}

//Function  for storing product detail
function store_user2(){
	
	localStorage.product = document.getElementById("product").value;
	localStorage.quantity=document.getElementById("quantity").value;
	
}

//Function for validating purchase.htm
function validate3(){
	var errMsg3 = "";								
	var result3 = true;
	var expiry = document.getElementById("cardexpire").value;
	var month = parseInt(expiry.substring(0, 2));
    var year  = parseInt(expiry.substring(3, 5));
  
	
	if((year == 14) && (month < 6)) 
	{
		errMsg3 += "Enter a valid expiry Credit Card Date\n";
		result3 = false;
   	}
	
	if((month < 1) || (month > 12)) 
	{
		errMsg3 += "Enter a valid Month\n";
		result3 = false;
	}
		
	else if((year < 14) || (year > 99)){
		errMsg3 += "Enter a valid expiry Credit Card Date\n";
		result3 = false;
	}	
	   
	if (errMsg3 != ""){   
		alert(errMsg3);
	}
	if (result3){ 
		var r=confirm("Are you sure you want to pay");
		if(r==false)
		{result3=false;}
		}
	return result3;
}

// Function for showing Customer and product detail on Purchase.htm
function show_data(){
	if(localStorage.name != undefined){
		var nameshow = document.getElementById("nameshow");
		nameshow.textContent="Name-:    " + localStorage.name;
		var dobshow = document.getElementById("dobshow");
		dobshow.textContent="DOB -:   " + localStorage.dob;
		var namestreet = document.getElementById("streetshow");
		namestreet.textContent=" Street -   " + localStorage.street;
		var namesuburb = document.getElementById("suburbshow");
		namesuburb.textContent="Suburb -  " + localStorage.suburb;
		var namestate = document.getElementById("stateshow");
		namestate.textContent="State -   " + localStorage.state;
		var namepostcode = document.getElementById("postcodeshow");
		namepostcode.textContent="Post Code-    " + localStorage.postcode;
		var phoneshow = document.getElementById("phoneshow");
		phoneshow.textContent="Phone -   " + localStorage.phone;
		var emailshow = document.getElementById("emailshow");
		emailshow.textContent="Email -  " + localStorage.email;
		var productshow = document.getElementById("productshow");
		productshow.textContent=localStorage.product;
		var quantityshow = document.getElementById("quantityshow");
		quantityshow.textContent=localStorage.quantity;
		var amountshow = document.getElementById("amountshow");
		if(localStorage.product == "Canon EOS 70D")
		{
		amountshow.textContent=" $ 1200 ";
		} 
		else {
		amountshow.textContent=" $ 1000 ";
		}
	}
}

/* Function to check if Billing Address is same as Delivery Address*/
function setDeliveryAddress(){
	if(document.getElementById("sameaddress").checked == true)
	{
		document.getElementById("dstreet").value = document.getElementById("bstreet").value;
		document.getElementById("dsuburb").value = document.getElementById("bsuburb").value;
		document.getElementById("dstate").value = document.getElementById("bstate").value;
		document.getElementById("dpostcode").value =document.getElementById("bpostcode").value;	
	}
	else 
	{

		document.getElementById("dstreet").value = '';
		document.getElementById("dsuburb").value = '';
		document.getElementById("dstate").value = '';
		document.getElementById("dpostcode").value = '';	
	}
}


function init () {

	var registration = document.getElementById("registration");
	if(registration){
		var checkaddress=document.getElementById("sameaddress");
		checkaddress.onclick = setDeliveryAddress;
		registration.onsubmit = validate;
		var select = document.getElementById("select");
		select.onclick = clicked;
	}
		
	var selection=document.getElementById("selection");
	if(selection){
		selection.onsubmit = validate2;
		var checkout = document.getElementById("checkout");
		checkout.onclick = clicked2;	
	}
	
	show_data();
	var purchase = document.getElementById("purchase");
	if(purchase){
	 purchase.onsubmit = validate3;
	 var span = document.getElementById('productshow');
	var hidden = document.getElementById('product');
	hidden.value = span.innerHTML;
	var span2 = document.getElementById('quantityshow');
	var hidden2 = document.getElementById('quantity');
	hidden2.value = span2.innerHTML;
	
	var quan = parseInt(span2.innerHTML);
		if(span.innerHTML == "Canon EOS 70D")
		{
		var amount= quan*1200;
		var hidden3 = document.getElementById('amount');
		hidden3.value = amount;
		} 
		else {
		var amount= quan*1000;
		var hidden3 = document.getElementById('amount');
		hidden3.value = amount;
		}
	
	
	 var previous = document.getElementById("previous");
		previous.onclick = clicked3;
	}


}

//Function for opening select.htm
function clicked()  {
    var b = validate(); 
    if (b) 
        window.open("select.php","_self")
    return b;
}

//Function for opening purchase.htm
function clicked2()  {
    var b = validate2(); 
    if (b) 
        window.open("purchase.php","_self")
    return b;
}

function clicked3()  {
    
        window.open("select.php","_self")
   
}




window.onload = init;

