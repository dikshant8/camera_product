<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<meta name="description" content="Internet Technologies Assignment-2" />
		<meta name="keywords" content="HTML, Javascript" />
		<meta name="author" content="Dikshant Bawa" />
		<link href= "styles/index.css" rel="stylesheet" type="text/css" />
		
		<script src="product.js" ></script>
		
		<!--  <link rel="stylesheet" type="text/css" media="only screen and (max-device-width: 640px)" href="styles/mobile.css" />  -->
		<title> Canon dSLR Camera </title>
	</head>
	<body>
		<?php
			include ("menu.php");
		?>
		<section>
		<h3> References </h3>
		
		<p>Content for Canon EOS70D <a class="ref" href="https://www.canon.com.au/en-AU/Personal/Products/Cameras-and-Accessories/EOS-Digital-SLR-Cameras/EOS-70D" target="_blank">link</a></p>
		<p>Content for Canon EOS60D <a class="ref" href="https://www.canon.com.au/en-AU/Personal/Products/Cameras-and-Accessories/EOS-Digital-SLR-Cameras/60D" target="_blank">link</a></p>
		<p>Backgorund Image <a class="ref" href="http://reasonerlandscaping.com/?attachment_id=51" target="_blank">link</a></p>
		<p>Background image for Mobile CSS <a class="ref" href="http://backgroundwhitegallery.blogspot.com.au/2013/08/photo-background-white.html" target="_blank">link</a></p>
		<p>Image of Canon EOS70D <a class="ref" href="http://www.digitaltrends.com/photography/dslr-cameras-show-theyre-still-kings-image-quality/" target="_blank">link</a></p>
		<p>Image of Canon EOS60D <a class="ref" href="http://lumanog-photos.blogspot.com.au/2012/07/cheapest-canon-eos-60d-for-sale-brand.html" target="_blank">link</a></p>
		<p>Home Wallpaper <a class="ref" href="http://www.wall321.com/Color/Selective_coloring/cameras_jackie_chan_canon_selective_coloring_1920x1200_wallpaper_57354/download_1280x1024" target="_blank">link</a></p>
		<p>Learning Guide <a class="ref" href="http://www.w3schools.com/" target="_blank">link</a></p>
		<p>A static reference to the desired jQuery file/ version <a class="ref" href="https://developers.google.com/speed/libraries/devguide#jquery" target="_blank">link</a></p>
		<p>Learning guide for using jQuery Library <a class="ref" href="https://learn.jquery.com/" target="_blank">link</a></p>
		
		</section>	
		<?php
			include ("footer.php");
		?>	
	</body>
</html>