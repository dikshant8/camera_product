<!DOCTYPE html><!-- Customer checking orders  -->
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<meta name="description" content="Internet Technologies Assignment-3" />
		<meta name="keywords" content="HTML, PHP" />
		<meta name="author" content="Dikshant Bawa" />
		<link href= "styles/index.css" rel="stylesheet" type="text/css" />
		<link href= "styles/form.css" rel="stylesheet" type="text/css" />
		<script src="product.js" ></script>
		<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
		<script src="alternative.js" ></script>-->
		<!--  <link rel="stylesheet" type="text/css" media="only screen and (max-device-width: 640px)" href="styles/mobile.css" />  -->
		<title> Canon dSLR Camera </title>
	</head>
	<body>
		<?php
			include ("menu.php");
		?>
		<form method="post" action="order_query.php">
		<fieldset id="detail2"><legend>Search your Orders</legend>
			<p>	<label for="cid">Customer ID </label>
				<input type="text" name="cid" id="cid" /></p>
			<p>	<input type="submit" value="Submit" />
				<input type="reset" value="Reset" /></p>
		</fieldset>
		</form>
		<?php
			if(isset($_POST['cid']))
			{
				$cid = $_POST['cid'];			
				require_once ("settings.php");
				$conn = @mysqli_connect($host,
						$user,
						$pwd,
						$sql_db
				);			
				//checking the connection
				if(!$conn){
					echo "<p> Database connection failure</p>";
				}
				else 
				{
					$query="select order_id,c_id,order_date,product,quantity,amount from orders where c_id='$cid' order by order_date"; 
					$result = mysqli_query($conn, $query);
					if(!($result)) {
						echo "<p> Something is wrong with",$query,"</p>";
					} 
					else{
						//Displaying the records
						echo"<table id='query'>";
						echo "<tr>"
							."<th scope=\"col\">Order ID</th>"
							."<th scope=\"col\">Customer ID</th>"
							."<th scope=\"col\">Order Date</th>"
							."<th scope=\"col\">Product</th>"
							."<th scope=\"col\">Quantity</th>"
							."<th scope=\"col\">Amount</th>"
						."</tr>";
						//retrieving Record from pointer
						while($row = mysqli_fetch_assoc($result)){
							echo "<tr>";
							echo "<td>",$row["order_id"],"</td>";
							echo "<td>",$row["c_id"],"</td>";
							echo "<td>",$row["order_date"],"</td>";
							echo "<td>",$row["product"],"</td>";
							echo "<td>",$row["quantity"],"</td>";
							echo "<td>",$row["amount"],"</td>";
							echo "</tr>";
						}
						echo "</table>";		
						mysqli_free_result($result);
					}	
					mysqli_close($conn);
				}	
			}
		?> 	
		<?php
			include ("footer.php");
		?>	
	</body>
</html>