<!DOCTYPE html><!-- Product Purchase Form -->
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<meta name="description" content="Internet Technologies Assignment-2" />
		<meta name="keywords" content="HTML, Javascript" />
		<meta name="author" content="Dikshant Bawa" />
		<link href= "styles/index.css" rel="stylesheet" type="text/css" />
		<link href= "styles/form.css" rel="stylesheet" type="text/css" />
		<script src="product.js" ></script>
		<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
		<script src="alternative.js" ></script>-->
		<!--  <link rel="stylesheet" type="text/css" media="only screen and (max-device-width: 640px)" href="styles/mobile.css" />  -->
		<title> Canon dSLR Camera </title>
	</head>
	<body >
	
		<?php
			include ("menu.php");
		?>
		<section>
		<p class="show"><span id="nameshow"></span></p>
		<p class="show"><span id="dobshow"></span></p>
		<p class="show">Address <br/><span id="streetshow"></span></p>
		<p class="show"><span id="suburbshow"></span></p>
		<p class="show"><span id="stateshow"></span></p>
		<p class="show"><span id="postcodeshow"></span></p>
		<p class="show"><span id="phoneshow"></span></p>
		<p class="show"><span id="emailshow"></span></p>
		<p class="show"><span id="productshow"></span></p>
		<p class="show"><span id="quantityshow"></span></p>
		<p class="show">Amount : - <span id="amountshow"></span></p>
		
			<!--Purchase Form-->
			<form id="purchase" method="post" action="order_add.php" >
				<fieldset class="detail"> 
					<legend id="mainlegend">Credit Card Payment</legend> 
					<p>
						<label for="credit">Credit Card </label>
							<select name="credit" id="credit">
								<option value="visa" selected="selected">Visa</option>
								<option value="master">MasterCard</option>
								<option value="american">American Express</option>
							</select> 
					</p> 
					<p>
						<label for="cardname">Name </label>
						<input type="text" name="cardname" id="cardname"  pattern="[A-Za-z]+" size="25" maxlength="30" required="required" />
					</p> 
					<p>	<label for="cardnumber">Number</label>
						<input type="text" name="cardnumber" id="cardnumber"  pattern="\d{16}" size="25" maxlength="16" required="required" />
					</p> 
					<p>	<label for="cardexpire">Expiry Date</label>
						<input type="text" id="cardexpire" name="cardexpire"  placeholder="mm-yy" pattern="\d{2}-\d{2}" maxlength="5" size="10" required="required"  />
					</p>	
					
					
					
					
					<input type="hidden" name="product" id="product" />
					<input type="hidden" name="quantity" id="quantity" />
					<input type="hidden" name="amount" id="amount" />

				</fieldset>
			    <p>
				  <input type="submit" value="Pay Now" />
				  <button type="button" id="previous" >Previous Page</button> 
				  <input type="reset" value="Reset" />
			    </p>
			</form>
			
		</section>	
		<?php
			include ("footer.php");
		?>	
	</body>
</html>