<!DOCTYPE html><!-- Customer PHP DATABASE STORING -->
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<meta name="description" content="Internet Technologies Assignment-3" />
		<meta name="keywords" content="HTML, PHP" />
		<meta name="author" content="Dikshant Bawa" />
		<link href= "styles/index.css" rel="stylesheet" type="text/css" />
		<link href= "styles/form.css" rel="stylesheet" type="text/css" />
		<script src="product.js" ></script>
		<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
		<script src="alternative.js" ></script>-->
		<!--  <link rel="stylesheet" type="text/css" media="only screen and (max-device-width: 640px)" href="styles/mobile.css" />  -->
		<title> Canon dSLR Camera </title>
	</head>
	<body>
	
		<?php
			include ("menu.php");
		?>
		<?php
			if(isset($_POST['firstname']))
			{
				require_once ("settings.php");
				$conn = @mysqli_connect($host,
					$user,
					$pwd,
					$sql_db
					);
				//checking the connection
				if(!$conn){
					echo "<p> Database connection failure</p>";
				}
				else {	
					//getting data from FORM
					$firstname = trim($_POST["firstname"]);
					$lastname = trim($_POST["lastname"]);
					$bstreet = trim($_POST["bstreet"]);
					$bsuburb = trim($_POST["bsuburb"]);
					$bstate = trim($_POST["bstate"]);
					$bpostcode = trim($_POST["bpostcode"]);
					$dstreet = trim($_POST["dstreet"]);
					$dsuburb = trim($_POST["dsuburb"]);
					$dstate = trim($_POST["dstate"]);
					$dpostcode = trim($_POST["dpostcode"]);
					$email = trim($_POST["email"]);
					$phone = trim($_POST["phone"]);
					
					$sql_table="customers";
					// creating table Customers if it doesnt exist
					$sqlString = "show tables like '$sql_table'"; 
					$result = @mysqli_query($conn, $sqlString);
					if(mysqli_num_rows($result)==0) {
						$sqlString = "create table customers (customer_id int not null auto_increment primary key,firstname varchar(20) not null,
						lastname varchar(20) not null,bstreet varchar(40) not null,bsuburb varchar(20) not null,bstate varchar(3) not null,
						bpostcode int(4) not null,dstreet varchar(40) not null,dsuburb varchar(20) not null,dstate varchar(3) not null,
						dpostcode int(4) not null,email varchar(30) not null, phone int(10) not null)"; 
						$result2 = @mysqli_query($conn, $sqlString);
					}
					else 
					{	
						//echo "<p>Table  $sql_table already exists</p>"; 
					} 		
					//SQL command for inserting the values
					$query="insert into $sql_table (firstname,lastname,bstreet,bsuburb,bstate,bpostcode,dstreet,dsuburb,dstate,dpostcode,email,phone)
					values ('$firstname','$lastname','$bstreet','$bsuburb','$bstate','$bpostcode','$dstreet','$dsuburb','$dstate','$dpostcode',
					'$email','$phone')";
					//executing the query
					if(!(($firstname=="") || ($lastname==""))) {
						if(strlen($phone) == 10) 
						{
							$result=mysqli_query($conn, $query);
							//checks if connection was successful
							if(!$result)
								echo "<p> Something is wrong with ",$query,"</p>";
							else 
							echo "<p> Successfully added new customer</p>";
							header('Location: https://mercury.ict.swin.edu.au/cos60004/s4942892/assign3/select.php');
						}
						else
							echo"<p> Enter a valid Phone Number</p>";
					}
					else
						echo"<p> Enter your Name</p>";
					mysqli_close($conn);
				}
			}
				
		?>	
		<a class="ref" href="customer.php">Go Back to Previous Page</a> 			
		<?php
			include ("footer.php");
		?>	
	</body>
</html>