<!DOCTYPE html><!-- VEndors Web Page  -->
<html lang="en">

	<head>
		<meta charset="utf-8" />
		<meta name="description" content="Internet Technologies Assignment-3" />
		<meta name="keywords" content="HTML, PHP" />
		<meta name="author" content="Dikshant Bawa" />
		<link href= "styles/index.css" rel="stylesheet" type="text/css" />
		<link href= "styles/form.css" rel="stylesheet" type="text/css" />
		<script src="product.js" ></script>
		<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
		<script src="alternative.js" ></script>-->
		<!--  <link rel="stylesheet" type="text/css" media="only screen and (max-device-width: 640px)" href="styles/mobile.css" />  -->
		<title> Canon dSLR Camera </title>
	</head>
	<body>
		<?php
			include ("menu.php");
		?>
		<?php
		session_start();
		$var=$_SESSION['login'];
		if( $var==1 )
		{
		
			
			require_once ("settings.php");
			$conn = @mysqli_connect($host,
					$user,
					$pwd,
					$sql_db
			);			
			//checking the connection
			if(!$conn){
				echo "<p> Database connection failure</p>";
			}
			else 
			{	
				$query="select * from orders"; 
				$result = mysqli_query($conn, $query);
				if(!($result)) {
					echo "<p> Something is wrong with",$query,"</p>";
				} 
				else{
					//Displaying the records
					echo"<table id='vendors' border=\"1\">";
					echo "<tr>"
					."<th scope=\"col\">Order ID</th>"
					."<th scope=\"col\">Customer ID</th>"
					."<th scope=\"col\">Card Name</th>"
					."<th scope=\"col\">Card Number</th>"
					."<th scope=\"col\">Card Expire</th>"
					."<th scope=\"col\">Order Date</th>"
					."<th scope=\"col\">Order Status</th>"
					."<th scope=\"col\">Product</th>"
					."<th scope=\"col\">Quantity</th>"
					."<th scope=\"col\">Amount</th>"
					."<th scope=\"col\">Delete</th>"
					."<th scope=\"col\">Update</th>"
					."</tr>";
					//retrieving Record from pointer
					while($row = mysqli_fetch_assoc($result)){
						$oid= $row['order_id'];
						echo "<form method='post' action='update.php?order_id=$oid'>";
					
						echo "<tr>";
						echo "<td>",$oid,"</td>";
						echo "<td>",$row["c_id"],"</td>";
						echo "<td>",$row["cardname"],"</td>";
						echo "<td>",$row["cardnumber"],"</td>";
						echo "<td>",$row["cardexpire"],"</td>";
						echo "<td>",$row["order_date"],"</td>";
						echo "<td>","<select name='status' id='status'>
							<option value=",$row['order_status'],">",$row['order_status'],"</option>
							<option value='pending'>pending</option>
							<option value='paid'>paid</option>
							<option value='fulfilled'>fulfilled</option>
							</select></td>";
										
						echo "<td>",$row["product"],"</td>";
						echo "<td>",$row["quantity"],"</td>";
						echo "<td>",$row["amount"],"</td>";
						echo "<td><a href='delete.php?order_id=$oid'>DELETE</a></td>";
						echo "<td><input type='submit' value='UPDATE'></td>";
						echo "</tr>";
						echo "</form>";
					}
					echo "</table>";		
					mysqli_free_result($result);
				}			
				mysqli_close($conn);	
			}	
		}	
		else{
		header('Location: https://mercury.ict.swin.edu.au/cos60004/s4942892/assign3/login.php');
		}
		
				
		?> 	
		


		<form method="post" action="vendors_page_filters.php">
		<fieldset id="detail2"><legend>Filter your Orders</legend>
			<p>	<label for="cid">Customer ID </label>
				<input type="text" name="cid" id="cid" /></p>
				<p>	<label for="date">Date </label>
				<input type="text" name="date" id="date"  placeholder="yyyy-mm-dd" pattern="\d{4}-\d{1,2}-\d{1,2}" maxlength="10" size="10" /></p>
			<p>	<input type="submit" value="Submit" />
				<input type="reset" value="Reset" /></p>
		</fieldset>
		</form>
		
		
		<?php
			include ("footer.php");
		?>	
	</body>
</html>