<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<meta name="description" content="Internet Technologies Assignment-3" />
		<meta name="keywords" content="HTML, PHP" />
		<meta name="author" content="Dikshant Bawa" />
		<link href= "styles/index.css" rel="stylesheet" type="text/css" />
		
		<script src="product.js" ></script>
		
		<!--  <link rel="stylesheet" type="text/css" media="only screen and (max-device-width: 640px)" href="styles/mobile.css" />  -->
		<title> Canon dSLR Camera </title>
	</head>
	<body>
	
		<?php
			include ("menu.php");
		?>
		<section>
			<p>Automatically created the orders and customers tables, if those tables do not already exist.</p>
			<p>Add pricing information to your products AND display a total cost of an order.</p>
			<p>Created a primary-foreign key link between the customers and orders tables. Ensure that an order cannot be created if a customer id is not valid</p>
			<p>Created a vendor registration page with server side validation requiring unique username and a password rule, and store this information in a table.<a href="vendor_registration.php">Vendor Registration</a></p>
			<p>Created a vendor log in page to use the stored information to control access to the vendor web site. Ensure the vendor web site cannot be entered directly using a URL.<a href="login.php">Login</a></p>
		</section>	
		<?php
			include ("footer.php");
		?>	
	</body>
</html>