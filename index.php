<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<meta name="description" content="Internet Technologies Assignment-2" />
		<meta name="keywords" content="HTML, Javascript" />
		<meta name="author" content="Dikshant Bawa" />
		<link href= "styles/index.css" rel="stylesheet" type="text/css" />
		
		<script src="product.js" ></script>
		
		<!--  <link rel="stylesheet" type="text/css" media="only screen and (max-device-width: 640px)" href="styles/mobile.css" />  -->
		<title> Canon dSLR Camera </title>
	</head>
	<body>
	
		<?php
			include ("menu.php");
		?>
		<section>
			<img class="wallpaper" src="images/camerasmall.jpg" alt="Canon Wallpaper" width="600" height="500" />
			<article class="feature">
				<p> Better image quality : <br/>
				 The dSLR’s sensor has larger pixels, which makes them more sensitive to light and less prone to grainy artifacts called noise.</p>
			</article>
			<article class="feature">
				<p> Lenses, lenses, and more lenses : <br/>
				 Lets you pop off the lens currently mounted on your camera and mount another one with different features.</p>
			</article>
			<article class="feature">
				<p> Bigger and Brighter view : <br/>
				The dSLR’s viewfinder shows you a large image of what the lens sees. </p>
			</article>
			<aside>
				<p> "Here is a qoute on digital camera by Demetri Martin : "</p>
				<blockquote cite="http://www.brainyquote.com/quotes/quotes/d/demetrimar414435.html">
				The digital camera is a great invention because it allows us to reminisce. Instantly.
				</blockquote>
				
			</aside>
		</section>	
		<?php
			include ("footer.php");
		?>	
	</body>
</html>