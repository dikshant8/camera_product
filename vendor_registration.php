<!DOCTYPE html><!-- VENDOR Registration PAGE Form -->
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<meta name="description" content="Internet Technologies Assignment-2" />
		<meta name="keywords" content="HTML, Javascript" />
		<meta name="author" content="Dikshant Bawa" />
		<link href= "styles/index.css" rel="stylesheet" type="text/css" />
		<link href= "styles/form.css" rel="stylesheet" type="text/css" />
		<script src="product.js" ></script>
		<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
		<script src="alternative.js" ></script>-->
		<!--  <link rel="stylesheet" type="text/css" media="only screen and (max-device-width: 640px)" href="styles/mobile.css" />  -->
		<title> Canon dSLR Camera </title>
	</head>
	<body>
	
		<?php
			include ("menu.php");
		?>
		<section>
			<!--Vendor Registration-->
			<form id="vendorregistration" method="post" action=" vendor_registration.php" >
				<fieldset class="detail"> 
					<legend id="mainlegend">Vendor Registration</legend> 
					<p>
						<label for="name">Name</label>
						<input type="text" name="name" id="name"  pattern="[A-Za-z]+" size="25" maxlength="20"  />
					 </p>  
					 <p><label for="email">Email address</label>
						<input type="email" placeholder= "your_email@example.org.me" id="email" name="email" />
					</p>	
					<p><label for="username">User Name</label>
						<input type="text" name="username" id="username"  pattern="[A-Za-z0-9]+" size="25" maxlength="20"  />
					 </p> 
					 <p><label for="pwd">Password</label>
						<input type="password" name="pwd" id="pwd"   size="25" maxlength="20"  />
					 </p> 
				</fieldset>
			    <p>
				  <input type="submit" value="Submit" />
				  
				  <input type="reset" value="Reset" />
			    </p>
			</form>
		<?php	
			if(isset($_POST['name']))
			{
				require_once ("settings.php");
				$conn = @mysqli_connect($host,
					$user,
					$pwd,
					$sql_db
					);
				//checking the connection
				if(!$conn){
					echo "<p> Database connection failure</p>";
				}
				else
				{	
					//getting data from FORM
					$name = trim($_POST["name"]);
					$email = trim($_POST["email"]);
					$username = trim($_POST["username"]);
					$pwd = trim($_POST["pwd"]);
					
					
					$sql_table="vendor";
					// creating table Customers if it doesnt exist
					$sqlString = "show tables like '$sql_table'"; 
					$result = @mysqli_query($conn, $sqlString);
					if(mysqli_num_rows($result)==0) {
						$sqlString = "create table vendor (vendor_id int not null auto_increment primary key,name varchar(20) not null,
						email varchar(20) not null,username varchar(40) not null,password varchar(20) not null)"; 
						$result2 = @mysqli_query($conn, $sqlString);
					}
					else 
					{	
						//echo "<p>Table  $sql_table already exists</p>"; 
					} 		
					//SQL command for inserting the values
					$query="insert into $sql_table (name,email,username,password)
					values ('$name','$email','$username','$pwd')";
					//executing the query
					$result=mysqli_query($conn, $query);
					//checks if connection was successful
					if(!$result)
						echo "<p> Something is wrong with ",$query,"</p>";
					else 
					{
						echo "<p> Successfully added </p>";
						//header('Location: https://mercury.ict.swin.edu.au/cos60004/s4942892/assign3/login.php');
					}
					mysqli_close($conn);
				}
			}
				
		?>	
			
		</section>	
		<?php
			include ("footer.php");
		?>	
	</body>
</html>