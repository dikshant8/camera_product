<!DOCTYPE html><!-- Product Selection Form -->
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<meta name="description" content="Internet Technologies Assignment-2" />
		<meta name="keywords" content="HTML, Javascript" />
		<meta name="author" content="Dikshant Bawa" />
		<link href= "styles/index.css" rel="stylesheet" type="text/css" />
		<link href= "styles/form.css" rel="stylesheet" type="text/css" />
		<script src="product.js" ></script>
		<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
		<script src="alternative.js" ></script>-->
		<!--  <link rel="stylesheet" type="text/css" media="only screen and (max-device-width: 640px)" href="styles/mobile.css" />  -->
		<title> Canon dSLR Camera </title>
	</head>
	<body>
	
		<?php
			include ("menu.php");
		?>
		<section>
			<!--Product Selection Form-->
			<form id="selection" method="post" action=" http://mercury.ict.swin.edu.au/it000000/formtest.php" >
				<fieldset class="detail"> 
					<legend id="mainlegend">Product Selection</legend>
					<p>
						<label for="product">Select Product</label>
							<select name="product" id="product">
								<option value="Canon EOS 60D" selected="selected">Canon EOS 60D</option>
								<option value="Canon EOS 70D">Canon EOS 70D</option>
							</select> 
					</p> 
					<p>
						<label for="quantity">Quantity</label>
							<select name="quantity" id="quantity">
								<option value="1" selected="selected">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
								<option value="4">4</option>
							</select>
					</p>
					<fieldset >	
						<legend class="addresslegend">Optional Accessories</legend>
							<label for="battery">Battery Grip</label>
							<input type="checkbox" name="access[]" id="battery" value="BATTERY"  /><br />
							<label for="tripod">Tripod Stand</label>
							<input type="checkbox" name="access[]" id="tripod" value="TRIPOD" /><br />
							<label for="hdmi">HDMI Cable</label>
							<input type="checkbox" name="access[]" id="hdmi" value="HDMI" /><br />
					</fieldset>
					<p> Camera Backpack
							<label for="yes">YES</label>
							<input type="radio" name="backpack" id="yes" class="option" value="Y" checked="checked"/>
							<label for="no">No</label>
							<input type="radio" name="backpack" id="no" class="option" value="N" />
					</p>		
				</fieldset>
			    <p>
				  <input type="submit" value="Submit" />
				 <!-- <a class="likeabutton" href="purchase.htm">Check Out</a>-->
				  <button type="button" id="checkout" >Check Out</button>
				  <input type="reset" value="Reset" />
			    </p>
			</form>
			
		</section>	
		<?php
			include ("footer.php");
		?>	
	</body>
</html>