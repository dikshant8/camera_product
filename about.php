<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<meta name="description" content="Internet Technologies Assignment-2" />
		<meta name="keywords" content="HTML, Javascript" />
		<meta name="author" content="Dikshant Bawa" />
		<link href= "styles/index.css" rel="stylesheet" type="text/css" />
		
		<script src="product.js" ></script>
		
		<!--  <link rel="stylesheet" type="text/css" media="only screen and (max-device-width: 640px)" href="styles/mobile.css" />  -->
		<title> Canon dSLR Camera </title>
	</head>
	<body>
	<?php
		include ("menu.php");
	?>
		
		<section>
			<img id="myphoto" src="images/Bawa.jpg" alt="My Photo" />
			 <p id="myname"> Dikshant Bawa</p>
			 <p id="swinid">Student ID:- 4942892</p>
			 <p id="course"> Master in Information Technology (Professional Computing) </p>
			 <h3 id="timetable"> Time Table </h3>
			 
			 <table id="timetable2" >
				<thead >
					<tr><th>Day</th> <th>Time</th><th>Subject</th><th>Location</th></tr>
				</thead>
				<tbody>
					<tr><td>Monday</td><td>10:30-12:30pm</td><td>COS60004_HS1_HAW Lab-Prac</td><td>ATC627</td></tr>
					<tr><td rowspan="2">Tuesday</td><td>12:30-14:30pm</td><td>COS70004_HS1_HAW LE1</td><td>BA302</td></tr>
					<tr><td>14:30-16:30pm</td><td>COS70004_HS1_HAW TU1</td><td>BA802/BA803</td></tr>
					<tr><td rowspan="2">Wednesday</td><td>10:30-12:30pm</td><td>COS60005_HS1_HAW LE1</td><td>EN102</td></tr>
					<tr><td>12:30-13:30pm</td><td>COS60005_HS1_HAW TU1</td><td>EN406</td></tr>
					<tr><td>Thursday</td><td>12:30-14:30pm</td><td>COS60004_HS1_HAW LE1</td><td>ATC101</td></tr>
				</tbody>
			</table>
			<p>	<strong>Favourite Films:</strong> Inception, Dark Knight, Wolf of Wall Street, Fight Club.</p>
			<p>	<strong>Favourite TV Series: </strong> Dexter, Game of Thrones, Lost, Breaking Bad, Prison Break, The Walking Dead, Big Bang Theory.</p>
			<p> <strong>Favourite Books: </strong> Immortals of Meluha, Tolkien Middle Earth</p>
			<p> <strong>Interests: </strong> Playing Football, Travelling.</p>
		</section>	
	<?php
		include ("footer.php");
	?>	
	</body>
</html>