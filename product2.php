<!DOCTYPE html> <!-- Content for EOS60d-->
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<meta name="description" content="Internet Technologies Assignment-2" />
		<meta name="keywords" content="HTML, Javascript" />
		<meta name="author" content="Dikshant Bawa" />
		<link href= "styles/index.css" rel="stylesheet" type="text/css" />
		
		<script src="product.js" ></script>
		
		<!--  <link rel="stylesheet" type="text/css" media="only screen and (max-device-width: 640px)" href="styles/mobile.css" />  -->
		<title> Canon dSLR Camera </title>
	</head>
	<body>
		<?php
			include ("menu.php");
		?>
		<section>
			
			<img class="wallpaper" src="images/eos60d3.jpg" alt="EOS 100D" width="500" height="350"/>
			<h2> Canon EOS 60D </h2>
			<ul id="tabs">
				<li><a href="#specs"> Detailed Specifications </a> </li>
				<li> <a href="#access"> Accessories </a></li>
			</ul>
			<div class="content" id="specs">
				<h3>Specifications </h3> 
				
				<table id="tablespecs" >      <!-- Table for Camera Specifications-->
					<tbody>
						<tr><td rowspan="5" class="point"><strong><em>Imaging</em></strong></td><td>Imaging Sensor/Effective Pixels</td><td>CMOS / 18.0 MP</td></tr>
						<tr><td>Effective Sensor Size</td><td>Approx 22.3x 14.9 mm</td></tr>
						<tr><td>A/D Resolution Power</td><td>14-bit</td></tr>
						<tr><td>Colour Filter Size</td><td>Primary Colours, APS-C</td></tr>
						<tr><td>35mm Focal Length Equivalent</td><td>1.6X</td></tr>
			
						<tr><td rowspan="6" class="point"><strong><em>Image Size</em></strong></td><td>Large</td><td>Approx. 17.90 megapixels (5184 x 3456)</td></tr>
						<tr><td>Medium</td><td>Approx. 8.00 megapixels (3456 x 2304)</td></tr>
						<tr><td>Small 1</td><td>Approx. 4.50 megapixels (2592 x 1728)</td></tr>
						<tr><td>Small 2</td><td>Approx. 2.5 megapixels (1920 x 1280)</td></tr>
						<tr><td>Small 3</td><td>Approx. 350,000 pixels (720 x 480)</td></tr>
						<tr><td>Raw</td><td>Approx. 17.9 megapixels (5184 x 3456)</td></tr>
						
						<tr><td rowspan="3" class="point"><strong><em>Movie Shooting (8GB Card)</em></strong></td><td>1920 x 1080 (FULL HD)</td><td>30/25/24 fps, 22min @330MB/min</td></tr>
						<tr><td>1280 x 720 (HD)</td><td>60/50fps, 22min @330MB/min</td></tr>
						<tr><td>720 x 480 (HD)</td><td>60/50fps,24 min@165MB/min</td></tr>
						
						<tr><td class="point"><strong><em>Speed</em></strong></td><td>Shooting Speed</td><td>5.3 fps</td></tr>
						
						<tr><td rowspan="2" class="point"><strong><em>Viewfinder</em></strong></td><td>Coverage (vertical/horizontal)</td><td>96%</td></tr>
						<tr><td>Dioptric Adjustment</td><td>-3 to +1m dpt</td></tr>
						
						<tr><td rowspan="2" class="point"><strong><em>White Balance</em></strong></td><td>Settings</td><td>8</td></tr>
						<tr><td>WB Bracketing</td><td>Yes</td></tr>
						
						<tr><td rowspan="8" class="point"><strong><em>General Features</em></strong></td><td>LCD Monitor</td><td>3" Wide Vari-Angle  </td></tr>
						<tr><td>Integrated Cleaning System</td><td>Yes</td></tr>
						<tr><td>Live View</td><td>Yes (Live View Shooting, Movie Servo AF, Remote Live View Shooting, Face Detection)</td></tr>
						<tr><td>External Interface</td><td>Hi-Speed USB/ HDMI / Stereo Audio OUT/ External Microphone IN </td></tr>
						<tr><td>Battery</td><td>Lithium Ion LP-E6</td></tr>
						<tr><td>Chassis Material</td><td>Plastic Resin</td></tr>
						<tr><td>Dimensions (W x H x D) mm</td><td>144.5 x 105.8 x 78.6 mm</td></tr>
						<tr><td>Weight excl. battery (g)</td><td>755</td></tr>
						
					</tbody>
				</table>
			</div>
			<div class="content" id="access">
				<h3> Optional Accessories for EOS 60D</h3>
				<ol>
					<li><strong><em>Speedlite 270EX II</em></strong> <br/>
					<p>A powerful and versatile flashgun that you can take anywhere</p></li>
					<li><strong><em>Original Data Security Kit OSK-E3</em></strong></li>
					<li><strong><em>Interface Cable IFC-500U</em></strong></li>
					<li><strong><em>Focusing Screen EF-A</em></strong></li>
					<li><strong><em>Hand Strap HS-E2</em></strong></li>
					<li><strong><em>Battery Grip BG-E9</em></strong></li>
				</ol>
			</div>
		</section>	
		<?php
			include ("footer.php");
		?>	
	</body>
</html>