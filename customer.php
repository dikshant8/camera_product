<!DOCTYPE html><!-- Customer Registration Form -->
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<meta name="description" content="Internet Technologies Assignment-2" />
		<meta name="keywords" content="HTML, Javascript" />
		<meta name="author" content="Dikshant Bawa" />
		<link href= "styles/index.css" rel="stylesheet" type="text/css" />
		<link href= "styles/form.css" rel="stylesheet" type="text/css" />
		<script src="product.js" ></script>
		<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
		<script src="alternative.js" ></script>-->
		<!--  <link rel="stylesheet" type="text/css" media="only screen and (max-device-width: 640px)" href="styles/mobile.css" />  -->
		<title> Canon dSLR Camera </title>
	</head>
	<body>
	
		<?php
			include ("menu.php");
		?>
		<section>
			<!--Registration Form-->
			<form id="registration" method="post" action=" customer_add.php" >
				<fieldset class="detail"> 
					<legend id="mainlegend">Customer Registration</legend> 
					<p>
						<label for="firstname">First name</label>
						<input type="text" name="firstname" id="firstname"  pattern="[A-Za-z]+" size="25" maxlength="20"  />
					 </p> 
					 <p><label for="lastname">Last name</label>
						<input type="text" name="lastname" id="lastname"  pattern="[A-Za-z]+" size="25" maxlength="20"  />
					 </p> 
					 <p><label for="dob">Date of Birth</label>
						<input type="text" id="dob" name="dob"  placeholder="dd/mm/yyyy" pattern="\d{1,2}/\d{1,2}/\d{4}" maxlength="10" size="10" required="required"  />
					</p>	
					<!--Billing Address-->
					<fieldset >
						<legend class="addresslegend">Billing Address</legend>
						<p><label for="bstreet">Street Address</label>
							<input type="text" name="bstreet" id="bstreet" maxlength="40"  required="required" />
						</p> 
						<p><label for="bsuburb">Suburb/Town</label>
							<input type="text" name="bsuburb" id="bsuburb" maxlength="20" required="required" />
						</p> 
						<p>
							<label for="bstate">State</label>
							<select name="bstate" id="bstate">
								<option value="VIC" selected="selected">VIC</option>
								<option value="NSW">NSW</option>
								<option value="QLD">QLD</option>
								<option value="NT">NT</option>
								<option value="WA">WA</option>
								<option value="SA">SA</option>
								<option value="TAS">TAS</option>
								<option value="ACT">ACT</option>
							</select>
						</p>
						<p>
							<label for="bpostcode">Postcode</label>
							<input type="text" id="bpostcode"  name="bpostcode"  placeholder="XXXX" pattern="\d{4}" maxlength="4" size="4" required="required"   />
						</p>						
					</fieldset>
					<!-- Delivery Address-->
					<fieldset >
						<legend class="addresslegend">Delivery Address</legend>
						<label id="chk" for="sameaddress">If Delivery Address is same as Billing Address</label>
						<input type="checkbox"  name="check" id="sameaddress"   /><br />
						<p><label for="dstreet">Street Address</label>
							<input type="text" name="dstreet" id="dstreet"  maxlength="40" required="required" />
						</p> 
						<p><label for="dsuburb">Suburb/Town</label>
							<input type="text" name="dsuburb" id="dsuburb" maxlength="20" required="required" />
						</p> 
						<p>
							<label for="dstate">State</label>
							<select name="dstate" id="dstate">
								<option value="VIC" selected="selected">VIC</option>
								<option value="NSW">NSW</option>
								<option value="QLD">QLD</option>
								<option value="NT">NT</option>
								<option value="WA">WA</option>
								<option value="SA">SA</option>
								<option value="TAS">TAS</option>
								<option value="ACT">ACT</option>
							</select>
						</p>
						<p>
							<label for="dpostcode">Postcode</label>
							<input type="text" id="dpostcode"  name="dpostcode"  placeholder="XXXX" pattern="\d{4}" maxlength="4" size="4" required="required"   />
						</p>						
					</fieldset>
					<p><label for="email">Email address</label>
						<input type="email" placeholder= "your_email@example.org.me" id="email" name="email" />
					</p>
					<p>
						<label for="phone">Phone number</label>
						<input type="text" id="phone"  name="phone"  placeholder="Enter 10 digit Number"   size="20" required="required"   />
					</p>
				</fieldset>
			  
			    <p>
				  <input type="submit" value="Submit" />
				  
				  <input type="reset" value="Reset" />
			    </p>
			</form>
			
		</section>	
		<?php
			include ("footer.php");
		?>	
	</body>
</html>