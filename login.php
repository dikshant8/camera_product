<!DOCTYPE html><!-- VENDOR LOG IN PAGE Form -->
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<meta name="description" content="Internet Technologies Assignment-2" />
		<meta name="keywords" content="HTML, Javascript" />
		<meta name="author" content="Dikshant Bawa" />
		<link href= "styles/index.css" rel="stylesheet" type="text/css" />
		<link href= "styles/form.css" rel="stylesheet" type="text/css" />
		<script src="product.js" ></script>
		<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
		<script src="alternative.js" ></script>-->
		<!--  <link rel="stylesheet" type="text/css" media="only screen and (max-device-width: 640px)" href="styles/mobile.css" />  -->
		<title> Canon dSLR Camera </title>
	</head>
	<body>
	
		<?php
			include ("menu.php");
		?>
		<section>
			<!--Vendor Login-->
			<form id="vendorregistration" method="post" action=" login.php" >
				<fieldset class="detail"> 
					<legend id="mainlegend">Login Form</legend> 	
					<p><label for="username">User Name</label>
						<input type="text" name="username" id="username"  pattern="[A-Za-z0-9]+" size="25" maxlength="20"  />
					 </p> 
					 <p><label for="pwd">Password</label>
						<input type="password" name="pwd" id="pwd"   size="25" maxlength="20"  />
					 </p> 
					 <input type="hidden" name="login" id="login" />
				</fieldset>
			    <p>
				  <input type="submit" value="Submit" />
				  
				  <input type="reset" value="Reset" />
			    </p>
			</form>
		<?php	
		
			if(isset($_POST['username']))
			{
				require_once ("settings.php");
				$conn = @mysqli_connect($host,
					$user,
					$pwd,
					$sql_db
					);
				//checking the connection
				if(!$conn){
					echo "<p> Database connection failure</p>";
				}
				else
				{	
					//getting data from FORM
					$username = trim($_POST["username"]);
					$pwd = trim($_POST["pwd"]);
					$get = "select username,password from vendor"; 
					$result = @mysqli_query($conn, $get);
					if(!$result){
						echo "<p> Something is wrong with</p>";
					}
					else{
					while($result1 = mysqli_fetch_assoc($result)){
						
						if($result1['username'] == $username){
							if($result1['password'] == $pwd){
								session_start();
								$_SESSION['login'] =1;
								header('Location: https://mercury.ict.swin.edu.au/cos60004/s4942892/assign3/vendors_page.php');
							}
							else{
								echo "<p> Invalid Username and Password</p>";
							}
						}
						else{
							echo "<p> Invalid Username and Password</p>"; 
						}
					}
					
					}
					mysqli_free_result($result);
					mysqli_close($conn);
				}
			}
				
		?>	
			
		</section>	
		<?php
			include ("footer.php");
		?>	
	</body>
</html>