<!DOCTYPE html> <!-- Enhance Ments Page-->
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<meta name="description" content="Internet Technologies Assignment-2" />
		<meta name="keywords" content="HTML, Javascript" />
		<meta name="author" content="Dikshant Bawa" />
		<link href= "styles/index.css" rel="stylesheet" type="text/css" />
		<link href= "styles/form.css" rel="stylesheet" type="text/css" />
		<script src="product.js" ></script>
		
		<!--  <link rel="stylesheet" type="text/css" media="only screen and (max-device-width: 640px)" href="styles/mobile.css" />  -->
		<title> Canon dSLR Camera </title>
	</head>
	<body>
	
		<?php
			include ("menu.php");
		?>
		<section >
			
		
			<h3> Canon EOS 70D</h3>
			<iframe class="video" width="560" height="315" src="//www.youtube.com/embed/SAHEZFQjNOw" ></iframe>
			<h3> Canon EOS 60D</h3>
			<iframe class="video" width="560" height="315" src="//www.youtube.com/embed/BW7n0wfHYek"  ></iframe>
			<h3> Canon Video</h3>
			<video  class="video" width="560" height="315" >
				<source src="media/canon2.mp4" type="video/mp4"/>
				
			</video>
			<h3> Audio </h3>
			<audio  class="video">
				
				 <source src="media/tune.mp3" type="audio/mp3"/>
				 Your browser does not support the audio element.
			</audio>
			
			<h3 id="svg">Scalable Vector Graphics </h3>
			<svg class="video" height="200">
				<circle id="redcircle" cx="50" cy="50" r="50" fill="red" />
			</svg>
			<h3>Css3 Animations</h3>
			<div id="d"></div>

		</section>
		<?php
			include ("footer.php");
		?>	
	</body>
</html>