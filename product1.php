<!DOCTYPE html> <!-- Content for EOS70d-->
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<meta name="description" content="Internet Technologies Assignment-2" />
		<meta name="keywords" content="HTML, Javascript" />
		<meta name="author" content="Dikshant Bawa" />
		<link href= "styles/index.css" rel="stylesheet" type="text/css" />
		
		<script src="product.js" ></script>
		
		<!--  <link rel="stylesheet" type="text/css" media="only screen and (max-device-width: 640px)" href="styles/mobile.css" />  -->
		<title> Canon dSLR Camera </title>
	</head>
	<body>
		<?php
			include ("menu.php");
		?>
		<section>
			
			<img class="wallpaper" src="images/dbs.jpg" alt="EOS 70D" width="500" height="350"/>
			<h2> Canon EOS 70D  </h2>
			<ul id="tabs">
				<li><a href="#specs"> Detailed Specifications </a> </li>
				<li> <a href="#access"> Accessories </a></li>
			</ul>
			<div class="content" id="specs">
				<h3>Specifications </h3> 
				
				<table id="tablespecs" >      <!-- Table for Camera Specifications-->
					<tbody>
						<tr><td rowspan="5" class="point"><strong><em>Imaging</em></strong></td><td>Imaging Sensor/Effective Pixels</td><td>CMOS / 20.2 MP</td></tr>
						<tr><td>Effective Sensor Size</td><td>Approx 22.5x 15.0 mm</td></tr>
						<tr><td>A/D Resolution Power</td><td>14-bit</td></tr>
						<tr><td>Colour Filter Size</td><td>Primary Colours, APS-C</td></tr>
						<tr><td>35mm Focal Length Equivalent</td><td>1.6X</td></tr>
			
						<tr><td rowspan="6" class="point"><strong><em>Image Size</em></strong></td><td>Large</td><td>Approx. 20 megapixels (5472 x 3648)</td></tr>
						<tr><td>Medium</td><td>Approx. 8.9 megapixels (3648 x 2432)</td></tr>
						<tr><td>Small 1</td><td>Approx. 5 megapixels (2736 x 1824)</td></tr>
						<tr><td>Small 2</td><td>Approx. 2.5 megapixels (1920 x 1280)</td></tr>
						<tr><td>Small 3</td><td>Approx. 350,000 pixels (720 x 480)</td></tr>
						<tr><td>Raw</td><td>Approx. 20 megapixels (5472 x 3648)</td></tr>
						
						<tr><td rowspan="3" class="point"><strong><em>Movie Shooting</em></strong></td><td>1920 x 1080 (FULL HD)</td><td>30p/25p/24p (IPB): approx 235 MB/min</td></tr>
						<tr><td>1280 x 720 (HD)</td><td>60p/50p (IPB): approx 205 MB/min</td></tr>
						<tr><td>640 x 480 (HD)</td><td>30p/25p (IPB): Approximately 78 MB/min)</td></tr>
						
						<tr><td class="point"><strong><em>Speed</em></strong></td><td>Shooting Speed</td><td>7 fps</td></tr>
						
						<tr><td rowspan="2" class="point"><strong><em>Viewfinder</em></strong></td><td>Coverage (vertical/horizontal)</td><td>98%</td></tr>
						<tr><td>Dioptric Adjustment</td><td>-3 to +1m dpt</td></tr>
						
						<tr><td rowspan="2" class="point"><strong><em>White Balance</em></strong></td><td>Settings</td><td>9</td></tr>
						<tr><td>WB Bracketing</td><td>Yes</td></tr>
						
						<tr><td rowspan="8" class="point"><strong><em>General Features</em></strong></td><td>LCD Monitor</td><td>3” Vari Angle TFT Colour Liquid Crystal</td></tr>
						<tr><td>Integrated Cleaning System</td><td>Yes</td></tr>
						<tr><td>Live View</td><td>Yes (Live View Shooting, Movie Servo AF, Remote Live View Shooting, Face Detection)</td></tr>
						<tr><td>External Interface</td><td>Hi-Speed USB/ HDMI / Stereo Audio OUT/ External Microphone IN </td></tr>
						<tr><td>Battery</td><td>Lithium Ion LP-E6</td></tr>
						<tr><td>Chassis Material</td><td>Aluminium alloy and polycarbonate resin with glass fibre</td></tr>
						<tr><td>Dimensions (W x H x D) mm</td><td>139 x 104.3 x 78.5 mm</td></tr>
						<tr><td>Weight incl. battery (g)</td><td>755</td></tr>
						
					</tbody>
				</table>
			</div>
			<div class="content" id="access">
				<h3> Optional Accessories for EOS 70D</h3>  
				<ol>
					<li><strong><em>Speedlite 600EX-RT</em></strong> <br/>
					<p>Highly reliable, high performance model that meets professional needs</p></li>
					<li><strong><em>Remote Switch RS-60E3</em></strong></li>
					<li><strong><em>Battery Grip BG-E14</em></strong> <br/>
					<p>Enjoy easier vertical shooting with the Battery Grip BG-E14</p></li>
					<li><strong><em>Macro Twin Lite MT-24EX</em></strong> <br/>
					<p>Highly flexible lighting system for close-up and creative</p></li>
					<li><strong><em>AC Adapter Kit ACK-E8</em></strong></li>
					<li><strong><em>HDMI Cable HTC-100</em></strong></li>
				</ol>
			</div>
		</section>	
		<?php
			include ("footer.php");
		?>	
	</body>
</html>