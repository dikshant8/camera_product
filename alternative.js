/**
* Author: Dikshant Bawa
* Purpose: This file is for validation of data for Assignment 2 using jQuery
* Created: 3 May 2014
* Last updated: 5 May 2014
*  
*/

//Function for validating customer.htm
function validate(){
	
	var errMsg = "";								
	var result = true;								
	var dob = $("#dob").val();
	var postcode = $("#bpostcode").val();
	var state = $("#bstate").val();
	
	/* Checking of Australian State PostCodes*/
	if (state == 'VIC')
	{
		if(( /^[38]\d\d\d/.test(postcode) )== false)
		{
			errMsg += "Enter a valid Postcode\n";
			result = false;
		}
	}
	else if (state == 'NSW')
	{
		if(( /^[12]\d\d\d/.test(postcode) )== false)
		{
			errMsg += "Enter a valid Postcode\n";
			result = false;
		}
	}
	else if (state == 'QLD')
	{
		if(( /^[49]\d\d\d/.test(postcode) )== false)
		{
			errMsg += "Enter a valid Postcode\n";
			result = false;
		}
	}
		
	else if (state == 'NT')
	{
		if(( /^[0]\d\d\d/.test(postcode) )== false)
		{
			errMsg += "Enter a valid Postcode\n";
			result = false;
		}
	}
	
	else if (state == 'WA')
	{
		if(( /^[6]\d\d\d/.test(postcode) )== false)
		{
			errMsg += "Enter a valid Postcode\n";
			result = false;
		}
	}
	
	else if (state == 'SA')
	{
		if(( /^[5]\d\d\d/.test(postcode) )== false)
		{
			errMsg += "Enter a valid Postcode\n";
			result = false;
		}
	}
	
	else if (state == 'TAS')
	{
		if(( /^[7]\d\d\d/.test(postcode) )== false)
		{
			errMsg += "Enter a valid Postcode\n";
			result = false;
		}
	}
	
	else if (state == 'ACT')
	{
		if(( /^[0]\d\d\d/.test(postcode) )== false)
		{
			errMsg += "Enter a valid Postcode\n";
			result = false;
		}
	}
	
	
/* Checking the DOB of Person is valid or not*/
	var day = parseInt(dob.substring(0, 2),10);
    var month  = parseInt(dob.substring(3, 5),10);
    var year  = parseInt(dob.substring(6, 10),10);

	if((month < 1) || (month > 12)) 
	{
		errMsg += "Enter a valid Month\n";
		result = false;
	}
	else if((day < 1) || (day > 31)){
		errMsg += "Enter a valid Day\n";
		result = false;
	}
		
	else if((year < 1) || (year > 9999)){
		errMsg += "Enter a valid Year\n";
		result = false;
	}	
	if (errMsg != ""){   
		alert(errMsg);
	}
	if (result){ 
		store_user(dob, state, postcode);
	}
	
	return result;   
}

//Function for storing Customer Details
function store_user(dob,state,postcode){
	
	localStorage.firstname = $("#firstname").val();
	localStorage.lastname = $("#lastname").val();
	localStorage.dob = dob;
	localStorage.state = state;
	localStorage.postcode = postcode;
	localStorage.street=$("#bstreet").val();
	localStorage.suburb=$("#bsuburb").val();
	localStorage.phone=$("#phone").val();
	localStorage.email=$("#email").val(); 
}

//Function for validating select.htm
function validate2(){
	var errMsg2 = "";								
	var result2 = true;
		
	if (( $("#battery").is( ":checked" )==false ) && ( $("#hdmi").is( ":checked" )==false ) && ( $("#tripod").is( ":checked" )==false ) ) {					
		errMsg2 += "Please select at least one optional accessory.\n";  
		result2 = false;
	}
	if (errMsg2 != ""){   
		alert(errMsg2);
	}
	if (result2){ 
		store_user2();
	}
	return result2;
}

//Function  for storing product detail
function store_user2(){
	
	localStorage.product = $("#product").val();
	localStorage.quantity=$("#quantity").val();
}

//Function for validating purchase.htm
function validate3(){
	var errMsg3 = "";								
	var result3 = true;
	var expiry = $("#cardexpire").val();
	var month = parseInt(expiry.substring(0, 2));
    var year  = parseInt(expiry.substring(3, 5));
  
	if((year == 14) && (month < 6)) 
	{
		errMsg3 += "Enter a valid expiry Credit Card Date\n";
		result3 = false;
   	}
	
	if((month < 1) || (month > 12)) 
	{
		errMsg3 += "Enter a valid Month\n";
		result3 = false;
	}
		
	else if((year < 14) || (year > 99)){
		errMsg3 += "Enter a valid expiry Credit Card Date\n";
		result3 = false;
	}	
	   
	if (errMsg3 != ""){   
		alert(errMsg3);
	}
	if (result3){ 
		var r=confirm("Are you sure you want to pay");
		if(r==false)
		{result3=false;}
	}
	return result3;
}

// Function for showing Customer and product detail on Purchase.htm
function show_data(){
	if(localStorage.firstname != undefined){
		$("#nameshow").text("Name-:    " + localStorage.firstname + " " + localStorage.lastname);
		$("#dobshow").text("DOB -:   " + localStorage.dob);
		$("#streetshow").text(" Street -   " + localStorage.street);
		$("#suburbshow").text("Suburb -  " + localStorage.suburb);
		$("#stateshow").text("State -   " + localStorage.state);
		$("#postcodeshow").text("Post Code-    " + localStorage.postcode);
		$("#phoneshow").text("Phone -   " + localStorage.phone);
		$("#emailshow").text("Email -  " + localStorage.email);
		$("#productshow").text("Product -   " + localStorage.product);
		$("#quantityshow").text("Quantity -   " + localStorage.quantity);
		if(localStorage.product == "Canon EOS 70D")
		{
		$("#amountshow").text(" $ 1200");
		} 
		else {
		$("#amountshow").text(" $ 1000");
		}
		
	}
}

/* Function to check if Billing Address is same as Delivery Address*/
function setDeliveryAddress(){
	if ($('#sameaddress').is(":checked"))
	{
		var street = $("#bstreet").val();
		$("#dstreet").val(street); 
		var suburb = $("#bsuburb").val();
		$("#dsuburb").val(suburb);
		var state = $("#bstate").val();
		$("#dstate").val(state);
		var postcode = $("#bpostcode").val();
		$("#dpostcode").val(postcode);
	}
	else 
	{
		$("#dstreet").val(''); 
		$("#dsuburb").val('');
		$("#dstate").val('VIC');
		$("#dpostcode").val('');	
	}
}


$(document).ready(function(){
	
	$('#registration').submit(validate);
	$('#sameaddress').click(setDeliveryAddress);
	$('#select').click(clicked);
	$('#selection').submit(validate2);
	$('#checkout').click(clicked2);	
	show_data();
	$('#purchase').submit(validate3); 
});

//Function for opening select.htm
function clicked()  {
    var b = validate(); 
    if (b) 
        window.open("select.htm","_self")
    return b;
}

//Function for opening purchase.htm
function clicked2()  {
    var b = validate2(); 
    if (b) 
        window.open("purchase.htm","_self")
    return b;
}